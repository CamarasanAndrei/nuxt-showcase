onmessage = (e) => {
  fetch(e.data.url).then(result => result.json())
    .then((order) => {
      postMessage({ order })
    })
    .catch((e) => {
      console.error('[ORDER WORKER]')
    })
}
