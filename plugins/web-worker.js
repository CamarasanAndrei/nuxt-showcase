import OrderWorker from '~/assets/js/order.worker.js'

export default (context, inject) => {
  inject('worker', {
    createOrderWorker () {
      return new OrderWorker()
    }
  })
}
