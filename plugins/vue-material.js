import Vue from 'vue'
import {
  MdApp,
  MdToolbar,
  MdDrawer,
  MdList,
  MdIcon,
  MdContent,
  MdButton,
  MdCard,
  MdSteppers,
  MdDivider,
  MdField,
  MdMenu,
  MdRadio,
  MdProgress,
  MdSnackbar,
  MdSpeedDial,
  MdBadge
} from 'vue-material/dist/components'

Vue.use(MdApp)
Vue.use(MdToolbar)
Vue.use(MdDrawer)
Vue.use(MdList)
Vue.use(MdIcon)
Vue.use(MdButton)
Vue.use(MdCard)
Vue.use(MdContent)
Vue.use(MdSteppers)
Vue.use(MdDivider)
Vue.use(MdField)
Vue.use(MdMenu)
Vue.use(MdRadio)
Vue.use(MdProgress)
Vue.use(MdSnackbar)
Vue.use(MdSpeedDial)
Vue.use(MdBadge)
